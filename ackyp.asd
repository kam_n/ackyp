;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-

;;; Copyright (c) 2019-2024, Kamen Tomov.  All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(asdf:defsystem #:ackyp
    :description "Interfacing AWS Glacier for backups"
    :author "Kamen Tomov"
    :version "0.0.1"
    :serial t
    :depends-on
    (:ironclad
     :yason
     :bordeaux-threads
     :s-base64
     :marshal
     :command-line-arguments
     :lib
     :persistent-objects)
    :components
    ((:module "lib"
              :components
              ((:file "packages")
               (:module "merkle"
                        :components
                        ((:file "merkle")
                         (:file "merkle-main")))
               (:module "timestamp-class"
                        :components
                        ((:file "timestamp")))))
     (:file "packages")
     (:module "istore"
              :components
              ((:file "vault")
               (:file "archive-description")
               (:file "upload-initiation")
               (:file "archive")
               (:file "multipart-part")
               (:file "job")))
     (:module "layer2"
              :components
              ((:module "commands"
                        :components
                        ((:file "commands-util")
                         (:file "job")
                         (:file "initiate-upload")
                         (:file "part-uploader")
                         (:file "part-downloader")
                         (:file "complete-upload")
                         (:file "delete-archive")))
               (:file "common-util")
               (:file "upload-worker")
               (:file "download-worker")
               (:file "rm")
               (:file "list-archives")
               (:file "jobs")
               (:file "cleanup")
               (:file "sync-inventory")))
     (:module "layer1"
              :components
              ((:file "start")
               (:file "list-archives")
               (:file "upload")
               (:file "rm")
               (:file "jobs")
               (:file "cleanup")
               (:file "sync-inventory")))))
