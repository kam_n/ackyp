all: clean buildapp build-sbcl

build-ecl:
	@ecl -load build.lisp

build-sbcl:
	sbcl --no-userinit --no-sysinit --non-interactive \
	    --load ~/quicklisp/setup.lisp \
        --eval '(ql:quickload "ackyp")' \
        --eval '(ql:write-asdf-manifest-file "/tmp/quicklisp-manifest.txt")'
	./buildapp --output ackyp  --manifest-file /tmp/quicklisp-manifest.txt \
        --load-system ackyp --eval '(defun spam (args) (tomov.kamen.ackyp::main (cdr args)))' \
        --entry spam

clean:
	@-rm ackyp buildapp *.fas* */*.fas* */*/*.fas* 2>/dev/null

install:
	sudo install ackyp /usr/bin

docker-setup:
	cp -a /Users/i332014/.aws ~
	cp -a /Users/i332014/.ackyp ~
	cp /Users/i332014/.eclrc ~
	sudo apt-get update
	sudo apt-get install ecl --yes
	curl https://beta.quicklisp.org/quicklisp.lisp -o ~/quicklisp.lisp
	ecl -load ~/quicklisp.lisp -eval '(progn (quicklisp-quickstart:install) (quit))'
	cp -r /Users/i332014/quicklisp ~
	echo export ACKYP_VAULT=kvault >> ~/.bashrc
	echo export ACKYP_PSIZE=4 >> ~/.bashrc
	echo export ACKYP_RAMDIR=/tmp >> ~/.bashrc
	echo export ACKYP_THREADS=10 >> ~/.bashrc
	echo export ACKYP_LOG_LEVEL=info >> ~/.bashrc
	echo 'please cd to ~/quicklisp/local-projects/ackyp'

buildapp:
	sbcl --no-userinit --no-sysinit --non-interactive \
	    --load ~/quicklisp/setup.lisp \
		--eval '(ql:quickload :buildapp)' \
        --eval '(buildapp:build-buildapp)'
