(ql:quickload :ackyp)

(asdf:make-build :ackyp
                 :type :program
                 :ld-flags '()
                 :move-here #P"."
                 :epilogue-code '(progn (tomov.kamen.ackyp:start-here))) 
(quit)
