(in-package :tomov.kamen.ackyp)

(with-open-file (*standard-input* "/home/kamen/Downloads/01-iosevka-2.3.3.zip" :element-type 'unsigned-byte)
  (sb-posix:setenv (symbol-name :ackyp_vnr) "vs1:eu-north-1" 1)
  (sb-posix:setenv (symbol-name :ackyp_psize) "8" 1)
  (sb-posix:setenv (symbol-name :ackyp_ramdir) "/ramdisk" 1)
  (sb-posix:setenv (symbol-name :ackyp_threads) "10" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("--up" "-" "--name" "7B53v15" "--type" "zip" "--package" "pkg")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_vnr) "pics:eu-north-1" 1)
  (get-vnr nil))

(progn
  (sb-posix:setenv (symbol-name :ackyp_vault) +vault+ 1)
  (sb-posix:setenv (symbol-name :ackyp_psize) "8" 1)
  (sb-posix:setenv (symbol-name :ackyp_ramdir) "/Volumes/ramdisk" 1)
  (sb-posix:setenv (symbol-name :ackyp_threads) "10" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("--up" "/Users/i332014/Downloads/SkypeForBusinessInstaller-16.28.0.116.pkg"
          "--type" "pkg")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_ramdir) "/Volumes/ramdisk" 1)
  (sb-posix:setenv (symbol-name :ackyp_threads) "10" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("--ur" "/Users/i332014/Downloads/SkypeForBusinessInstaller-16.28.0.116.pkg" "--id" "1594")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_sort) "" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("--ls")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_scope) "" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main `("--rm" "--id" "22")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_log_level) "debug" 1)
  (main '("--fu")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_log_level) "debug" 1)
  (main '("--cleanup"))

(progn
  (sb-posix:setenv (symbol-name :ackyp_vnr) "vs1:eu-north-1" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("--iir")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("--iar" "--id" "1388")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_vnr) "dublin:eu-west-1" 1)
  (sb-posix:setenv (symbol-name :ackyp_log_level) "debug" 1)
  (main '("--lj")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (main '("-r" "--id" "34")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_log_level) "info" 1)
  (sb-posix:setenv (symbol-name :ackyp_psize) "1" 1)
  (sb-posix:setenv (symbol-name :ackyp_threads) "10" 1)
  (main '("-r" "--id" "99" "--dir" "/tmp")))

(progn
  (sb-posix:setenv (symbol-name :ackyp_log_level) "debug" 1)
  (sb-posix:setenv (symbol-name :ackyp_vault) +vault+ 1)
  (main '("--sync")))
