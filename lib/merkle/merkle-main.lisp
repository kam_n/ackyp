(in-package #:tomov.kamen.merkle)

(defun read-sequence-bin (seq stream &key (start 0) (end (length seq)))
  (file-position stream start)
  (do ((byte (read-byte stream nil) (read-byte stream nil))
       (i 0 (1+ i)))
      ((not (and byte (< i end))) (subseq seq 0 i))
    (setf (aref seq i) byte)))

(defun read-chunks-collect-result (stream fsize range-start result leaf-size fn)
  (if (>= range-start fsize)
      (reverse result)
      (read-chunks-collect-result
       stream
       fsize
       (+ range-start leaf-size)
       (cons (funcall fn
              (read-sequence-bin
               (make-array leaf-size :element-type '(unsigned-byte 8)) stream :start range-start))
             result)
       leaf-size
       fn)))

(defun prepare-and-traverse-merkle-tree (fname leaf-size traversf concatf)
  (let (fsize)
    (values
     (traverse-merkle-tree
      (with-open-file (s fname :element-type '(unsigned-byte 8))
        (setq fsize (file-length s))
        (read-chunks-collect-result s fsize 0 nil leaf-size traversf))
      traversf
      concatf)
     fsize)))
