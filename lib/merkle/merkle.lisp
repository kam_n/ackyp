(in-package #:tomov.kamen.merkle)

(defun traverse-merkle-tree (lst traversf concatf)
  (if (second lst)
      (traverse-merkle-tree
       (let (result)
         (do* ((lst lst (cddr lst))
               (h1 (first lst) (first lst))
               (h2 (second lst) (second lst)))
              ((not h2) (reverse
                         (if h1
                             (push h1 result)
                             result)))
           (push (funcall traversf (funcall concatf h1 h2))
                 result)))
       traversf
       concatf)
      (car lst)))
