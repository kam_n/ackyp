(in-package #:tomov.kamen.timestamp)

(defclass timestamp ()
  ((datetime :initarg :datetime :initform (get-universal-time)
             :accessor timestamp-datetime :type number)))

(defmethod print-object ((object timestamp) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "timestamp: ~A" (timestamp-datetime object))))
