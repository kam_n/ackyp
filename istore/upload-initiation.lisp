(in-package #:tomov.kamen.ackyp.istore)

(defclass upload-initiation (vault)
  ((upload-id :initarg :upload-id :reader upload-initiation-upload-id :type string)
   (part-size :initarg :part-size :reader upload-initiation-part-size :type number)
   (description-obj :initarg :description :reader upload-initiation-description-obj
                    :type archive-description)))

(def-persistent-object upload-initiation *instance*)

(defun process-upload-initiations (process-fn vnr)
  (mapcar process-fn
          (sort (find-all-with-vnr
                 (find-all-objects *instance* 'upload-initiation)
                 vnr)
                #'< :key #'get-id)))
