(in-package #:tomov.kamen.ackyp.istore)

(defclass vault (object-with-id)
  ((name :initarg :name :reader vault-name :type string)
   (region :initarg :region :reader vault-region :type string)))

(defmethod print-object ((object vault) stream)
  (print-bound-object-slots object stream))

(defun vnr-equal (vnr1 vnr2)
  (and (equal (vault-name vnr1) (vault-name vnr2))
       (equal (vault-region vnr2) (vault-region vnr2))))

(defun find-all-with-vnr (lst vnr)
  (remove-if-not #'(lambda (elem)
                     (vnr-equal elem vnr))
                 lst))
