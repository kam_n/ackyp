(in-package #:tomov.kamen.ackyp.istore)

(defclass multipart-part (object-with-id timestamp)
  ((initiation-id :initarg :initiation-id :reader multipart-part-initiation-id
                  :type number)
   (start :initarg :start :reader multipart-part-start :type number)
   (size :initarg :size :reader multipart-part-size :type number)
   (checksum :initarg :checksum :reader multipart-part-checksum :type string)
   (upload-speed-kbps :initarg :upload-speed-kbps :reader
                      multipart-part-upload-speed-kbps :type number)))

(def-persistent-object multipart-part)

(defun find-all-multiparts (id)
  (find-objects-with-slots *instance* 'multipart-part `((initiation-id ,id eql))))
