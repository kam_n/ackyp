(in-package #:tomov.kamen.ackyp.istore)

(defclass job-base (vault timestamp)
  ((location :initarg :location :reader job-base-location :type string)
   (job-id :initarg :job-id :reader job-base-id :type string)))

(defclass job-inventory (job-base) ())

(defclass job-archive (job-base)
  ((archive-id :initarg :archive-id :reader job-archive-id :type number)))

(def-persistent-object job-archive)
(def-persistent-object job-inventory)
