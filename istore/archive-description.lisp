(in-package #:tomov.kamen.ackyp.istore)

(defclass archive-description ()
  ((name :initarg :name :reader archive-description-name :type string)
   (datetime :initarg :datetime :initform (get-universal-time) :reader archive-description-datetime :type number)
   (type :initarg :type :reader archive-description-type :type string)
   (package :initarg :package :reader archive-description-package :initform "." :type string)))

(defmethod print-object ((object archive-description) stream)
  (print-bound-object-slots object stream))

(defmethod ms:class-persistent-slots ((self archive-description))
  '(name datetime type package))

(defun base64-encode (string)
  (with-output-to-string (out)
    (s-base64:encode-base64-bytes (map 'vector #'char-code string) out)))

(defun base64-decode (string)
  (handler-case (map 'string
       #'code-char
       (with-input-from-string (in string)
         (s-base64:decode-base64-bytes in)))
    (error (err) err)))

(defgeneric marshal-and-base64encode (archive-description)
  (:method ((archive-description archive-description)) 
    (remove
     #\Newline
     (base64-encode
      (format nil "~s" (ms:marshal archive-description))))))

(defun unmarshal-and-debase64 (base64string)
  (ms:unmarshal
   (read-from-string
    (base64-decode base64string))))
