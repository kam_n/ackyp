(in-package #:tomov.kamen.ackyp.istore)

(defclass archive (vault)
  ((glacier-id :initarg :archive-id :reader archive-id :type string)
   (size :initarg :size :initform -1 :reader archive-size :type number)
   (checksum :initarg :checksum :initform "undefined" :reader archive-checksum
             :type string)
   (description-obj :initarg :description :reader archive-description-obj
                    :type archive-description)))

(def-persistent-object archive)

(defun find-archives-by-vnr (vnr)
  (find-objects-with-slots
   *instance*
   'archive
   `((name ,(vault-name vnr))
     (region ,(vault-region vnr)))))
