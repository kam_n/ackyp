(in-package #:tomov.kamen.ackyp.layer2)

(defun rm-internal (archive-id scope)
  (let ((archive (find-object-with-id *instance* 'archive archive-id)))
    (case (intern (string-upcase scope) "KEYWORD")
      (:local (delete-persistent *instance* archive))
      (:cloud (delete-archive-cloud-command archive))
      (otherwise (progn
                   (delete-persistent *instance* archive)
                   (delete-archive-cloud-command archive))))))
