(in-package #:tomov.kamen.ackyp.layer2)

(defvar +glacier-inventory+ "inventory")

(defvar +job-expiration-timeout-seconds+ (* 2 24 60 60))

(defun delete-multiparts (obj-id)
  (mapcar
   #'(lambda (particle)
       (delete-persistent *instance* particle))
   (find-all-multiparts obj-id)))

(defun upload-clean-up (init-obj-id)
  (delete-multiparts init-obj-id)
  (delete-persistent
   *instance*
   (find-object-with-id *instance* 'upload-initiation init-obj-id)))

(defun get-ackyp-directory ()
  (let ((pname (pathname (getenv :ackyp_config_dir))))
    (append (pathname-directory pname) (list (pathname-name pname)))))

(defun get-glacier-inventory-file (vnr)
  (make-pathname :name (format nil "~a_~a_~a" +glacier-inventory+
                               (vault-name vnr) (vault-region vnr))
                 :type "json" :directory (get-ackyp-directory)))

(defun find-particle (parts start size)
  (handler-case 
      (find-if #'(lambda (part)
                   (and (= start (multipart-part-start part))
                        (= size (multipart-part-size part))))
               parts)
    (error () (log-info "start: ~a size: ~a" start size))))

(defmacro boolean-result (arg)
  `(not (not ,arg)))

(let ((counter 0))
  (defun display-progress ()
    (if (zerop (mod (incf counter) 10))
        (format t " ~a " counter)
        (progn (write-char #\+)
               (force-output)))))
