(in-package #:tomov.kamen.ackyp.layer2)

(defun get-job-output (job-inventory)
  (boolean-result
   (get-job-output-command
    job-inventory
    (get-glacier-inventory-file job-inventory))))

(defun initiate-job-archive-retrieval (archive)
  (boolean-result (initiate-job-archive-retrieval-cmd archive)))
