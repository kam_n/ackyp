(in-package #:tomov.kamen.ackyp.layer2)

(defun complete-multipart-upload (upload-initiation size checksum)
  (invoke-aws-process
   (list "complete-multipart-upload"
         (vault-name upload-initiation)
         (vault-region upload-initiation)
         "--cli-input-json"
         (yason:with-output-to-string* ()
           (yason:with-object ()
             (yason:encode-object-element
              "uploadId"
              (upload-initiation-upload-id upload-initiation))
             (yason:encode-object-element "archiveSize" size)
             (yason:encode-object-element "checksum" checksum))))))

(define-condition incomplete-transfer (condition) ())

(defun is-transfer-finished (parts)
  (when parts
    (reduce #'(lambda (result new)
                (and result (multipart-part-checksum new)))
            parts :initial-value t)))

(defun total-size (parts)
  (reduce '+ parts :key 'multipart-part-size))

(defun sha256treehash (parts)
  (calc-full-merkle-hash
   (mapcar 'multipart-part-checksum
           (sort parts '< :key #'multipart-part-start))))

(defun complete-upload (init-obj-id)
  (let* ((init-obj (find-object-with-id *instance* 'upload-initiation init-obj-id))
         (parts (find-all-multiparts init-obj-id)))
    (if (is-transfer-finished parts)
        (let ((size (total-size parts)))
          (multiple-value-bind (location checksum archive-id)
              (log-result '("location" "checksum" "archiveId")
                          (complete-multipart-upload
                           init-obj
                           (format nil "~a" size)
                           (sha256treehash parts)))
            (declare (ignore location))
            (make-persistent
             *instance*
             (make-instance
              'archive
              :archive-id archive-id
              :size size
              :checksum checksum
              :description (get-description-from-upload-initiation init-obj)
              :name (vault-name init-obj)
              :region (vault-region init-obj)))))
        (error (make-condition 'incomplete-transfer)))))

(defun get-description-from-upload-initiation (init-obj)
  (let ((init-descr-obj (upload-initiation-description-obj init-obj)))
    (make-instance 'archive-description
                   :name (archive-description-name init-descr-obj)
                   :type (archive-description-type init-descr-obj)
                   :package (archive-description-package init-descr-obj))))
