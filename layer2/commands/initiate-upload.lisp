(in-package #:tomov.kamen.ackyp.layer2)

(defun initiate-multipart-upload (vnr description-obj psize-bytes)
  (let ((result
         (invoke-aws-process
          (list "initiate-multipart-upload"
                (vault-name vnr)
                (vault-region vnr)
                "--archive-description" (marshal-and-base64encode description-obj)
                "--part-size" (format nil "~a" psize-bytes)))))
    (multiple-value-bind (location upload-id)
        (log-result '("location" "uploadId") result)
      (declare (ignore location))
      (make-persistent
       *instance*
       (make-instance 'upload-initiation
                      :name (vault-name vnr)
                      :region (vault-region vnr)
                      :upload-id upload-id
                      :part-size psize-bytes
                      :description description-obj)))))
