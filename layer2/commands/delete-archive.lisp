(in-package #:tomov.kamen.ackyp.layer2)

(defun delete-archive-cloud-command (archive)
  (log-result
   nil
   (invoke-aws-process
    (list "delete-archive" (vault-name archive) (vault-region archive)
          "--cli-input-json"
          (yason:with-output-to-string* ()
            (yason:with-object ()
              (yason:encode-object-element "archiveId" (archive-id archive))))))))
