(in-package #:tomov.kamen.ackyp.layer2)

(defun initiate-job-inventory-retrieval-cmd (vnr)
  (let ((vault (vault-name vnr))
        (region (vault-region vnr)))
    (multiple-value-bind (location job-id)
        (log-result
         '("location" "jobId")
         (invoke-aws-process
          (list "initiate-job" (vault-name vnr) (vault-region vnr)
                "--job-parameters"
                (yason:with-output-to-string* ()
                  (yason:with-object ()
                    (yason:encode-object-element "Type" "inventory-retrieval"))))))
      (make-persistent
       *instance*
       (make-instance
        'job-inventory
        :location location
        :job-id job-id
        :datetime (get-universal-time)
        :region region
        :name vault)))))

(defun initiate-job-archive-retrieval-cmd (archive)
  (let ((vault (vault-name archive))
        (region (vault-region archive)))
    (multiple-value-bind (location job-id)
        (log-result
         '("location" "jobId")
         (invoke-aws-process
          (list "initiate-job" vault region
                "--job-parameters"
                (yason:with-output-to-string* ()
                  (yason:with-object ()
                    (yason:encode-object-element "Type" "archive-retrieval")
                    (yason:encode-object-element "ArchiveId" (archive-id archive)))))))
      (make-persistent
       *instance*
       (make-instance
        'job-archive
        :location location
        :job-id job-id
        :name vault
        :region region
        :datetime (get-universal-time)
        :archive-id (get-id archive))))))

(defun get-job-output-command (job outfile)
  (let ((res (invoke-aws-process
              (list "get-job-output" (vault-name job) (vault-region job)
                    (format nil "--job-id=~a" (job-base-id job))
                    (enough-namestring outfile)))))
    (prog1 res
      (maphash
       #'(lambda (key val)
           (log-debug "~a : ~a" key val))
       res))))
