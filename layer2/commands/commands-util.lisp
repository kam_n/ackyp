(in-package #:tomov.kamen.ackyp.layer2)

(defvar +transfer-retries+ 3)

(defun log-result (keys result)
  (values-list
   (mapcar #'(lambda (key)
               (let ((value (gethash key result)))
                 (log-debug "~a: ~a" key value)
                 value))
           keys)))

(defun traversf (byte-array)
  (ironclad:digest-sequence :sha256 byte-array))

(defun concatf (el1 el2)
  (concatenate '(vector (unsigned-byte 8)) el1 el2))

(defun calc-full-merkle-hash (lst)
  (and lst (ironclad:byte-array-to-hex-string
            (traverse-merkle-tree
             (mapcar #'ironclad:hex-string-to-byte-array lst)
             #'traversf
             #'concatf))))

(defun ensure-process-exit-code (process)
  (unless (zerop (process-exit-code process))
    (error
     (make-condition
      'process-condition
      :code (process-exit-code process)
      :error (collect-process-outcomes process)))))

(defun aws-process-f (process args)
  (declare (ignore args))
  (process-wait process)
  (ensure-process-exit-code process)
  (let ((res (collect-process-outcomes process)))
    (yason:parse
     (format nil "~{~a~}" (or res '("{}"))))))

(defun simple-process-f (process args)
  (declare (ignore args))
  (process-wait process)
  (ensure-process-exit-code process)
  (collect-process-outcomes process))

(defun invoke-aws-process (args)
  (invoke-process #'run-aws args #'aws-process-f))

(let ((leaf-size (* 1024 1024)))
  (defun calc-file-merkle-hash (fname)
    (multiple-value-bind (checksum fsize)
        (prepare-and-traverse-merkle-tree
         fname
         leaf-size
         #'traversf
         #'concatf)
      (values (ironclad:byte-array-to-hex-string checksum)
              fsize))))

(defun calc-transfer-speed (start-time timestamp size)
  (let ((divisor (- timestamp start-time)))
    (round (/ size 1024)
           (if (zerop divisor) 0.01 divisor))))
