(in-package #:tomov.kamen.ackyp.layer2)

(defun download-chunk (job fname start fsize)
  (handler-case
      (invoke-aws-process
       (list "get-job-output" (vault-name job) (vault-region job)
             (format nil "--job-id=~a" (job-base-id job))
             "--range" (format nil "bytes=~a-~a" start (+ start fsize -1))
             (enough-namestring fname)))
    (process-condition (condition)
      (prog1 nil
        (log-error "~a" (process-condition-error condition))))
    (simple-type-error (condition)
      (prog1 nil
        (log-error "~a" condition)))))

(defun download-multipart-part (job start size fname)
  (labels ((controller (retries)
             (let* ((result (download-chunk job fname start size))
                    (checksum (and result (log-result '("checksum") result))))
               (if (and result
                        (equal checksum (calc-file-merkle-hash fname))
                        (= size (with-open-file (s fname)
                                  (file-length s))))
                   t
                   (progn
                     (log-error "Error downloading archive. Retrying ~a" (namestring fname))
                     (if (= retries +transfer-retries+)
                         (return-from controller nil)
                         (controller (1+ retries))))))))
    (let ((start-time (get-universal-time)))
      (when (controller 1)
        (calc-transfer-speed start-time (get-universal-time) size)))))
