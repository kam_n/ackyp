(in-package #:tomov.kamen.ackyp.layer2)

(defun upload-chunk (upload-initiation fname start fsize)
  (let ((pretty-fname (enough-namestring fname)))
    (make-thread
     #'(lambda ()
         (handler-case
             (invoke-aws-process
              (list "upload-multipart-part"
                    (vault-name upload-initiation)
                    (vault-region upload-initiation)
                    "--range" (format nil "bytes ~a-~a/*" start (+ start fsize -1))
                    "--body" pretty-fname
                    "--cli-input-json"
                    (yason:with-output-to-string* ()
                      (yason:with-object ()
                        (yason:encode-object-element "uploadId" (upload-initiation-upload-id upload-initiation))))))
           (process-condition (condition)
             (prog1 nil
               (log-error "~a" (process-condition-error condition))))
           (simple-type-error (condition)
             (prog1 nil
               (log-error "~a" condition)))))
     :name (format nil "~a upload thread" pretty-fname))))

(defun upload-multipart-part (upload-initiation start size fname result-lock)
  (labels ((uploaded? (result checksum)
             (and result (equal checksum (log-result '("checksum") result))))
           (controller (retries &optional checksum)
             (let ((thread (upload-chunk upload-initiation fname start size)))
               (unless checksum
                 (setq checksum (calc-file-merkle-hash fname)))
               (let ((result (join-thread thread)))
                 (unless (uploaded? result checksum)
                   (log-error "Checksum error during upload. Retrying ~a" (namestring fname))
                   (if (= retries +transfer-retries+)
                       (return-from controller nil)
                       (controller (1+ retries) checksum)))
                 (when (uploaded? result checksum) checksum))))
           (add-result (upload-speed-kbps datetime checksum)
             (bt:with-lock-held (result-lock)
               (make-persistent
                *instance*
                (make-instance 'multipart-part
                               :initiation-id (cl-prevalence:get-id upload-initiation)
                               :start start
                               :size size
                               :checksum checksum
                               :upload-speed-kbps upload-speed-kbps
                               :datetime datetime))
               (display-progress))))
    (let ((start-time (get-universal-time))
          (checksum (controller 1)))
      (when checksum
        (let ((end-time (get-universal-time)))
          (add-result (calc-transfer-speed start-time end-time size)
                      end-time
                      checksum))))))
