(in-package #:tomov.kamen.ackyp.layer2)

(defun write-input-stream-to-file (stream fname seq)
  (with-open-file (out fname
                       :direction :output
                       :element-type 'unsigned-byte
                       :if-exists :supersede)
    (let ((position (read-sequence seq stream)))
      (write-sequence seq out :end position)
      position)))

(defun safely-delete (fexists fname result-lock)
  (when fexists
    (handler-case (delete-file fname)
      (file-error (condition)
        (bt:with-lock-held (result-lock) ; remove lock once log oprations are thread safe
          (log-error "Error while deleting ~a: ~a" (enough-namestring fname)
                     condition))))))

(defun upload-worker (stream threads ramdir upload-init-obj)
  (let ((psize (upload-initiation-part-size upload-init-obj))
        (result-lock (bt:make-lock))
        (input-stream-lock (bt:make-lock))
        (parts (find-all-multiparts (get-id upload-init-obj)))
        (counter 0)
        (gstart 0)
        eof)
    (let ((seq (make-array psize :element-type 'unsigned-byte)))
      (flet ((add-transfer-job (fname)
               (make-thread
                #'(lambda ()
                    (let (fexists)
                      (do () (eof (safely-delete fexists fname result-lock))
                        (handler-case
                            (multiple-value-bind (start size)
                                (bt:with-lock-held (input-stream-lock)
                                  (unless eof
                                    (let ((position (write-input-stream-to-file stream fname seq)))
                                      (multiple-value-prog1 (values gstart position)
                                        (setq eof (< position psize)
                                              gstart (* (incf counter) psize))))))
                              (unless (or (not start) (find-particle parts start size)) ; start is null when eof
                                (setq fexists t)
                                (upload-multipart-part upload-init-obj start size fname result-lock)))
                          (t (condition) (progn (setq eof t) (log-error "~a" condition))))))))))
        (mapcar
         #'join-thread
         (loop for i from 1 to threads
            collect (add-transfer-job
                     (make-pathname :directory ramdir
                                    :name (upload-initiation-upload-id upload-init-obj)
                                    :type (princ-to-string i)))))))))

(defun upload-and-complete (threads ramdir init-obj-id)
  (upload-worker *standard-input* threads ramdir
                 (find-object-with-id *instance* 'upload-initiation init-obj-id))
  (complete-upload init-obj-id)
  (upload-clean-up init-obj-id))
