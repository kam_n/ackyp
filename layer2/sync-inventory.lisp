(in-package #:tomov.kamen.ackyp.layer2)

(defun parse-aws-date (date)
  (flet ((psubseq (date start end)
           (parse-integer (subseq date start end))))
    (encode-universal-time
     (psubseq date 17 19)
     (psubseq date 14 16)
     (psubseq date 11 13)
     (psubseq date 8 10)
     (psubseq date 5 7)
     (psubseq date 0 4))))

(defun get-description-obj (description)
  (handler-case (unmarshal-and-debase64 description)
    (error () nil)))

(defun make-archive-internal (entry vnr)
  (make-persistent
   *instance*
   (make-instance
    'archive
    :archive-id (gethash "ArchiveId" entry)
    :size (gethash "Size" entry)
    :checksum (gethash "SHA256TreeHash" entry)
    :description (get-description-obj (gethash "ArchiveDescription" entry))
    :name (vault-name vnr)
    :region (vault-region vnr))))

(defun sync-inventory-internal (vnr)
  "Based on glacier inventory file creates local db entries for all missing archives. To prevent inconsistencies prior to execution ensure the inventory file is up to date. An inconsistency will happen if the inventory was fetched prior to archive removal."
  (let ((all-inventory-count 0)
        (missing-inventory-count 0))
    (mapcar #'(lambda (entry)
                (incf all-inventory-count)
                (unless (find-if #'(lambda (archive)
                                     (equal archive (gethash "ArchiveId" entry)))
                                 (find-all-objects *instance* 'archives)
                                 :key #'archive-id)
                  (incf missing-inventory-count)
                  (make-archive-internal entry vnr)))
            (gethash
             "ArchiveList"
             (with-open-file (stream (get-glacier-inventory-file vnr))
               (yason:parse stream))))
    (values missing-inventory-count all-inventory-count)))
