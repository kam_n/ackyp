(in-package #:tomov.kamen.ackyp.layer2)

(defun decode-sort-criteria (criteria)
  (case (intern (string-upcase criteria) "KEYWORD")
    (:name (values #'string-lessp
                   #'(lambda (archive)
                       (let ((adescr (archive-description-obj archive)))
                         (handler-case (archive-description-name adescr)
                           (error () ""))))))
    (:size (values #'<
                   #'(lambda (archive)
                       (archive-size archive))))
    (:time (values #'<
                   #'(lambda (archive)
                       (let ((adescr (archive-description-obj archive)))
                         (handler-case (archive-description-datetime adescr)
                           (error () 0))))))
    (otherwise (values #'< #'get-id))))

(defun get-sorted-list-archives (lst criteria)
  (multiple-value-bind (predicate key) (decode-sort-criteria criteria)
    (sort lst predicate :key key)))
