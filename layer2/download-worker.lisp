(in-package #:tomov.kamen.ackyp.layer2)

(defun prefix-pathname (pathspec string &optional (delimiter "_"))
  (merge-pathnames
   (make-pathname :name (concatenate 'string string delimiter
                                     (pathname-name pathspec)))
   pathspec))

(defun write-chunk-to-destination (out chunk-name seq)
  (with-open-file (in chunk-name :element-type '(unsigned-byte 8))
    (let ((position (read-sequence seq in)))
      (write-sequence seq out :end position)
      position)))

(defun get-fs-suitable-name (archive directory)
  (let ((description (archive-description-obj archive)))
    (flet ((get-name ()
             (let* ((nm (archive-description-name description))
                    (len (length nm))
                    changed-p)
               (when (>= len 256)
                 (setq nm (subseq nm 0 255)
                       changed-p t))
               (when (or (zerop len) (find-if-not #'graphic-char-p nm))
                 (setq nm (princ-to-string (gensym))
                       changed-p t))
               (when changed-p
                 (log-error "Invalid archive name: ~a. Downloading to: ~a instead"
                            (archive-description-name description) nm))
               (pathname nm))))
      (let ((fname (get-name)))
        (if directory
            (merge-pathnames directory fname)
            fname)))))

(defun download-worker (job threads directory psize ramdir)
  (let* ((archive (find-object-with-id *instance* 'archive (job-archive-id job)))
         (fpath (get-fs-suitable-name archive directory))
         (temp-fpath (prefix-pathname fpath "downloading"))
         (archive-size (archive-size archive))
         (remaining archive-size)
         (job-id (get-id job))
         (result-lock (bt:make-lock))
         (input-lock (bt:make-lock))
         (parts (find-all-multiparts job-id))
         (start 0)
         (counter 0)
         eof
         (seq (make-array psize :element-type 'unsigned-byte)))
    (labels ((prepare-transfer ()
               (bt:with-lock-held (input-lock)
                 (unless eof
                   (let ((size (min remaining psize))
                         (chunk-pathspec
                          (make-pathname :directory ramdir
                                         :name "downloaded"
                                         :type (princ-to-string start))))
                     (multiple-value-prog1 (values (find-particle parts start size)
                                                   start size chunk-pathspec)
                       (setq start (* (incf counter) psize)
                             eof (/= size psize))
                       (decf remaining size))))))
             (add-transfer-job ()
               (with-open-file (out temp-fpath
                                    :direction :output
                                    :element-type '(unsigned-byte 8)
                                    :if-exists :overwrite
                                    :if-does-not-exist :create)
                 (do () (eof)
                   (multiple-value-bind (particle start size chunk-name)
                       (prepare-transfer)
                     (unless (or particle (not start))
                       (let ((download-speed-kbps
                              (download-multipart-part job start size chunk-name)))
                         (when download-speed-kbps
                           (bt:with-lock-held (result-lock)
                             (file-position out start)
                             (write-chunk-to-destination out chunk-name seq)
                             (make-persistent
                              *instance*
                              (make-instance 'multipart-part
                                             :initiation-id job-id
                                             :start start
                                             :size size
                                             :checksum ""
                                             :upload-speed-kbps download-speed-kbps
                                             :datetime 1))
                             (display-progress)))
                         (handler-case (delete-file chunk-name)
                           (error (condition) (log-debug "~a" condition))))))))))
      (when (probe-file fpath)
        (error (format nil "File ~a already exists!" (enough-namestring fpath))))
      (format t "~&")
      (mapcar #'join-thread (loop repeat threads collect (make-thread #'add-transfer-job)))
      (if (= (total-size (find-all-multiparts job-id))
             archive-size)
          (let ((res (equal (archive-checksum archive)
                            (calc-file-merkle-hash temp-fpath))))
            (if res
                (progn
                  (rename-file temp-fpath fpath)
                  (delete-multiparts job-id))
                (log-error "Checksum error when retrieving job ~a in file ~a"
                           job-id (namestring fpath)))
            res)
          (log-error "Expected archive size ~a differs from actual ~a"
                     archive-size (total-size parts))))))
