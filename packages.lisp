;;;; package.lisp

(defpackage #:tomov.kamen.ackyp.istore
  (:use
   #:cl #:tomov.kamen.timestamp
   #:tomov.kamen.process-manipulation
   #:tomov.kamen.persistent-objects)

  (:export
   #:*instance*

   #:vault #:vault-name #:vault-region #:vnr-equal #:find-all-with-vnr

   #:upload-initiation
   #:upload-initiation-location
   #:upload-initiation-upload-id
   #:upload-initiation-part-size
   #:upload-initiation-description-obj
   #:process-upload-initiations

   #:archive
   #:archive-id
   #:archive-size
   #:archive-checksum
   #:archive-description-obj
   #:find-archives-by-vnr

   #:archive-description-name
   #:archive-description-datetime
   #:archive-description-type
   #:archive-description-package

   #:archive-description
   #:marshal-and-base64encode
   #:unmarshal-and-debase64
   #:base64-decode
   #:base64-encode

   #:multipart-part
   #:multipart-part-initiation-id
   #:multipart-part-start
   #:multipart-part-size
   #:multipart-part-checksum
   #:multipart-part-upload-speed-kbps
   #:find-all-multiparts

   #:job-base-id
   #:job-inventory
   #:job-archive-id
   #:job-archive

   ))

(defpackage #:tomov.kamen.ackyp.layer2
  (:use
   #:cl #:bordeaux-threads
   #:tomov.kamen.os
   #:tomov.kamen.process-manipulation
   #:tomov.kamen.ackyp.istore
   #:tomov.kamen.logging
   #:tomov.kamen.timestamp
   #:tomov.kamen.merkle
   #:tomov.kamen.persistent-objects)
  (:export
   #:clean-uploads #:clean-downloads #:clean-expired-jobs
   #:delete-duplicate-archives
   #:upload-and-complete
   #:initiate-multipart-upload
   #:get-job-output
   #:initiate-job-archive-retrieval
   #:initiate-job-inventory-retrieval-cmd
   #:rm-internal #:archive-retrieval-job-exists
   #:sync-inventory-internal
   #:get-sorted-list-archives
   #:download-worker
   #:get-glacier-inventory-file
   #:boolean-result
   #:get-ackyp-directory))

(defpackage #:tomov.kamen.ackyp
  (:use
   #:cl #:bordeaux-threads
   #:tomov.kamen.process-manipulation
   #:tomov.kamen.logging
   #:tomov.kamen.timestamp
   #:tomov.kamen.ackyp.istore
   #:tomov.kamen.ackyp.layer2
   #:tomov.kamen.persistent-objects)
  (:export
   #:start-here))

(defvar tomov.kamen.ackyp.istore:*instance*)
