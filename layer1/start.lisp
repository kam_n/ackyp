(in-package #:tomov.kamen.ackyp)

(defparameter +command-line-spec+
  '((("up" #\u) :type string
     :documentation
     "Uploads an archive. Expects file name or dash (-) for the standard input and optionally --name and --type")
    (("ur") :type string
     :documentation
     "Resumes an unfinished upload. Expects file name or dash (-) for the standard input and --id")
    (("fu") :type boolean :documentation "Lists all unfinished uploads")
    (("ls" #\l) :type boolean :documentation "Lists all archives")
    ("rm" :type boolean :documentation "Removes an archive. Expects --id")
    ("rmall" :type boolean :documentation "Removes all archives from the vnr.")
    ("cleanup" :type boolean :documentation "Cleans-up. Optimizes the database")
    (("iir") :type boolean :documentation "Initiates inventory retrieval")
    (("iar") :type boolean :documentation
     "Initiate archive retrieval. Expects --id")
    (("lj") :type boolean :documentation "Lists all retrieval jobs")
    (("rt" #\r) :type boolean :documentation
     "Retrieves the output of a job. Expects --id and a --dir in case this is a retrieval of an archive")
    (("sync") :type boolean :documentation "Syncs the archive inventory")

    ("name" :type string :documentation "Uploaded file name")
    ("type" :type string :documentation "Uploaded file type")
    ("package" :type string :documentation "Uploaded file package info")
    ("dir" :type string :documentation "Download destination directory")
    ("id" :type integer :documentation "ID number")
    (#\v :type boolean :documentation "Verbose")
    ("scope" :type string :documentation
     "Removal scope. Can be local, cloud or both")
    ("vnr" :type string :documentation
     "Glacier vault and region in \"vault:region\" format.")
    ("sort" :type string :documentation
     "List archive sorting method. Can be name, size or default.")
    ("psize" :type string :documentation "Chunk size. Must be a power of 2.")
    ("parallel" :type string :documentation
     "Number of parallel upload/download operations.")
    ("ramdir" :type string :documentation
     "A directory, preferrably in the RAM for temp files.")))

(defvar *copyright-message*
"
 Kamen Tomov (c) 2019-2022
")

#+ecl
(setq ext:*help-message* *copyright-message*)

(defun start-here () 
  #+ecl
  (let ((ext:*lisp-init-file-list* NIL)) ; No initialization files
    (handler-case (ext:process-command-args :rules +ackyp-rules+)
      (error (c)
        (princ :*copyright-message* *error-output*)
        (quit 1)))))

(defun print-help (stream)
  (command-line-arguments:show-option-help +command-line-spec+)
  (princ *copyright-message* stream))

(defvar +ackyp-rules+
  '(("*DEFAULT*" 1 (quit :unix-status (main 1)) :stop)))

(defun get-keyword-from-str (str)
  (intern (string-upcase str) "KEYWORD"))

(defun get-keyword-from-env (envname default-val)
  (let ((envalue (getenv envname)))
    (if envalue
        (get-keyword-from-str envalue)
        default-val)))

(defun set-log-level (&optional log-level)
  (setq tomov.kamen.logging::+default-log-level+ 
        (case (or log-level (get-keyword-from-env :ackyp_log_level :info))
          (:debug tomov.kamen.logging::+debug+)
          (:info tomov.kamen.logging::+info+)
          (:error tomov.kamen.logging::+error+)
          (otherwise tomov.kamen.logging::+error+))))

(defun calc-chunks-num (fname psize)
  (ceiling (with-open-file (in fname :element-type 'unsigned-byte)
           (file-length in))
         psize))

(defun start-upload (up name type package vnr psize-envvar parallel ramdir)
  (let ((psize (get-psize-bytes psize-envvar)))
    (if (equal "-" up)
        (upload name type package psize vnr parallel ramdir)
        (with-open-file (*standard-input* up :element-type 'unsigned-byte)
          (log-info "Uploading ~a chunks of size ~aMB.~%"
                    (calc-chunks-num up psize) psize-envvar)
          (upload
           (enough-namestring (or name up))
           (or type (pathname-type up))
           package
           psize
           vnr
           parallel
           ramdir))))
  (snapshot *instance*))

(defun start-upload-resume (ur id psize parallel ramdir)
  (let ((psize (get-psize-bytes psize)))
    (if (equal "-" ur)
        (upload-resume id parallel ramdir)
        (let ((total-chunks (calc-chunks-num ur psize))
              (uploaded-chunks (length (find-all-multiparts id))))
          (with-open-file (*standard-input* ur :element-type 'unsigned-byte)
            (log-info
             "Uploading ~a chunks of total ~a (already uploaded ~a), ~aMB each.~%"
                      (- total-chunks uploaded-chunks)
                      total-chunks uploaded-chunks psize)
            (upload-resume id parallel ramdir))))))

(defun get-vnr (vnr)
  (let ((vnr (or vnr (getenv :ackyp_vnr))))
    (flet ((err ()
             (error "~S is not a valid VNR" vnr)))
      (if vnr
          (progn
            (let ((lst (kamo-lib:split-by-one-char vnr #\:)))
              (if (and lst (second lst))
                  (make-instance 'vault :name (first lst) :region (second lst))
                  (err))))
          (err)))))

(defun ackyp-function (&key up ur fu ls rm rmall cleanup iir iar lj rt sync dir
                         name type package id
                         v scope vnr sort psize parallel ramdir)
  (if v
    (set-log-level :debug)
    (set-log-level))
  (flet ((get-vnr ()
           (get-vnr vnr))
         (get-psize ()
           (or psize (getenv :ackyp_psize "8")))
         (get-parallel ()
           (or parallel (getenv :ackyp_threads "10")))
         (get-sort ()
           (or sort (get-keyword-from-env :ackyp_sort :other)))
         (get-ramdir ()
           (or ramdir (getenv :ackyp_ramdir "/tmp"))))
    (cond (up (start-upload up name type package (get-vnr) (get-psize)
                            (get-parallel) (get-ramdir)))
          (ur (start-upload-resume ur id (get-psize) (get-parallel) (get-ramdir)))
          (fu (find-uploads (get-vnr)))
          (ls (ls (get-sort) (get-vnr)))
          (rm (rm id (if scope
                         (get-keyword-from-str scope)
                         (get-keyword-from-env :ackyp_scope :all))))
          (rmall (rmall (get-vnr)))
          (cleanup (cleanup (get-vnr)))
          (iir (boolean-result (initiate-job-inventory-retrieval-cmd (get-vnr))))
          (iar (initiate-job-archive-retrieval
                (find-object-with-id *instance* 'archive id)))
          (lj (list-jobs (get-vnr)))
          (rt (retrieve id dir (get-psize) (get-parallel) (get-ramdir)))
          (sync (sync-inventory (get-vnr)))))
  (format t "~&"))

(defun main (args) 
  (with-storage (*instance* :dir (make-pathname :directory (get-ackyp-directory)))
    (handler-case
        (command-line-arguments:handle-command-line
         ;; the spec as above, or prepared with prepare-command-line-options-specification
         +command-line-spec+
         ;; the function to call with the arguments as parsed
         'ackyp-function
         ;; the arguments to parse
         :command-line args
         ;; the program name to use in case of an error message
         :name "ackyp"
         ;; the number of mandatory positional arguments for this command (default: 0)
         :positional-arity 0
         ;; What to do with the rest of the positional arguments.
         ;; T means pass the list of the rest of the command-line-arguments as one lisp argument.
         ;; NIL means ignore it. A keyword means pass this rest as a keyword argument.
         :rest-arity nil)
      (t (condition)
        (log-error "~a" condition)
        (print-help t)
        2)))) 

(defun format-date (date)
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time date)
    (format nil "~4,'0d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d"
            year month date hour minute second)))

(defun is-psize-valid (psize)
  (let ((val psize))
    (not (or (< val 2)
             (/= val (expt 2 (round (log val 2))))))))

(defun get-psize-bytes (psize)
  (let ((psize-bytes (* (parse-integer psize) 1024 1024)))
    (unless (is-psize-valid psize-bytes)
      (error (make-condition
              'condition
              :error "The part size should be a power of 2")))
    psize-bytes))
