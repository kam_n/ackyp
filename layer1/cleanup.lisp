(in-package #:tomov.kamen.ackyp)

(defun cleanup (vnr)
  "Cleans all unfinished uploads, the corresponding upload initiations as well as expired jobs. Makes a snapshot."
  (let* ((slots-and-values
          `((tomov.kamen.ackyp.istore::name ,(vault-name vnr))
            (tomov.kamen.ackyp.istore::region ,(vault-region vnr))))
         (archive-jobs
          (find-objects-with-slots *instance* 'job-archive slots-and-values))
         (inventory-jobs
          (find-objects-with-slots *instance* 'job-inventory slots-and-values)))
    (log-info "Deleted ~a upload initiations"
              (clean-uploads vnr))
    (log-info "Deleted ~a download parts"
              (clean-downloads archive-jobs))
    (log-info "Deleted ~a archive jobs"
              (clean-expired-jobs archive-jobs))
    (log-info "Deleted ~a inventory jobs"
              (clean-expired-jobs inventory-jobs))
    (log-info "Deleted ~a duplicates"
              (delete-duplicate-archives vnr))
    (snapshot *instance*)))
