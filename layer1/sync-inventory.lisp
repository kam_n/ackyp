(in-package #:tomov.kamen.ackyp)

(defun sync-inventory (vnr)
  (multiple-value-bind (missing-inventory-count all-inventory-count)
      (sync-inventory-internal vnr)
    (log-info "Inventorized ~a archives among ~a."
              missing-inventory-count all-inventory-count)
    t))
