(in-package #:tomov.kamen.ackyp)

(let* ((one 1024.0)
       (mil (* one 1024))
       (bil (* mil 1024)))
  (defun format-size (size)
    (cond ((> size bil) (format nil "~5,1fG" (/ size bil)))
          ((> size mil) (format nil "~5,1fM" (/ size mil)))
          ((> size (* 100 one)) (format nil "~5,1fK" (floor size one)))
          (t (format nil "~6d" size)))))

(defun describe-archive (archive-description)
  (handler-case
      (flet ((empty-if-nil (arg) (or arg "")))
        (format nil "~a ~a: ~a ~a"
                (format-date (archive-description-datetime archive-description))
                (empty-if-nil (archive-description-package archive-description))
                (empty-if-nil (archive-description-name archive-description))
                (empty-if-nil (archive-description-type archive-description))))
    (error (condition) (prog1 "" (log-error "~a" condition)))))

(defun ls (sort vnr)
  "Lists all uploaded archives by the specified criteria"
  (mapcar
   #'(lambda (archive)
       (format
        t
        "~6d ~a ~a~%"
        (get-id archive)
        (format-size (archive-size archive))
        (describe-archive (archive-description-obj archive))))
   (get-sorted-list-archives (find-archives-by-vnr vnr) sort))
  t)
