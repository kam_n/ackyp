;;;; upload.lisp

(in-package #:tomov.kamen.ackyp)

(defun find-uploads (vnr)
  "Finds all unfinished uploads."
  (process-upload-initiations
   #'(lambda (upload)
       (let ((adescr (upload-initiation-description-obj upload))
             (upload-id (get-id upload)))
         (format
          t
          "~6d ~a ~3d ~a~%"
          upload-id
          (format-date (archive-description-datetime adescr))
          (length (find-all-multiparts upload-id))
          (archive-description-name adescr))))
   vnr)
  t)

(defun upload-resume (id parallel ramdir)
  "Resumes an upload."
  (upload-and-complete
   (parse-integer parallel)
   ramdir
   id))

(defun upload (name type package psize vnr parallel ramdir)
  (upload-and-complete
   (parse-integer parallel)
   ramdir
   (get-id
    (initiate-multipart-upload
     vnr
     (make-instance 'archive-description :name name :type type :package package)
     psize))))
