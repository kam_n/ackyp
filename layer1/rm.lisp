(in-package #:tomov.kamen.ackyp)

(defun rm (archive-id scope)
  "Removes an archive. Expects id parameter as it is in _ls_"
  (if (find-object-with-slot
       *instance*
       'job-archive
       'tomov.kamen.ackyp.istore::archive-id
       archive-id)
      (log-error "Will not delete as retrieval job exists for archive ~a"
                 archive-id)
      (boolean-result (or (rm-internal archive-id scope) t))))

(defun rmall (vnr)
  "Removes all archives"
  (mapcar
   #'(lambda (archive)
       (format
        t
        "Deleting ~6d ~a ~a~%"
        (get-id archive)
        (format-size (archive-size archive))
        (describe-archive (archive-description-obj archive)))
       (rm (get-id archive) :all))
   (find-archives-by-vnr vnr)))
