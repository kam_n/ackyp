(in-package #:tomov.kamen.ackyp)

(defun list-jobs (vnr)
  "Lists all jobs."
  (let ((slots-and-values
         `((tomov.kamen.ackyp.istore::name ,(vault-name vnr))
           (tomov.kamen.ackyp.istore::region ,(vault-region vnr)))))
    (mapcar
     #'(lambda (job)
         (format
          t
          "~6d ~a ~a~%"
          (get-id job)
          (format-date (timestamp-datetime job))
          (class-name (class-of job))))
     (sort (find-objects-with-slots *instance* 'job-inventory slots-and-values)
           #'< :key #'get-id))
    (mapcar
     #'(lambda (job)
         (format
          t
          "~&~6d ~a ~a ~a~&"
          (get-id job)
          (format-date (timestamp-datetime job))
          (class-name (class-of job))
          (job-archive-id job)))
     (sort (find-objects-with-slots *instance* 'job-archive slots-and-values)
           #'< :key #'get-id))
    t))

(defun retrieve (id dir psize parallel ramdir)
  (let ((job-archive (find-object-with-id *instance* 'job-archive id))
        (job-inventory (find-object-with-id *instance* 'job-inventory id)))
    (cond
      (job-archive (retrieve-archive job-archive dir psize parallel ramdir))
      (job-inventory (get-job-output job-inventory))
      (t (log-error "No such job")))))

(defun retrieve-archive (job-archive dir psize-envvar parallel ramdir)
  "Retrieves the job output in :fpath environment variable. Requires archive retrieval job-id parameter as returned by list-jobs."
  (let* ((psize (get-psize-bytes psize-envvar))
         (total-chunks (ceiling
                        (archive-size (find-object-with-id
                                       *instance*
                                       'archive
                                       (job-archive-id job-archive)))
                        psize))
         (downloaded-chunks (length (find-all-multiparts (get-id job-archive)))))
    (log-info
     "Downloading ~a chunks of total ~a (already downloaded ~a). ~aMB each."
              (- total-chunks downloaded-chunks)
              total-chunks
              downloaded-chunks
              psize-envvar)
    (download-worker job-archive
                     (parse-integer parallel)
                     (if dir (pathname (concatenate 'string dir "/")))
                     psize
                     ramdir)))
